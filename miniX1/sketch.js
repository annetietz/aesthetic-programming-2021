var img;

function preload() {
  img = loadImage("snurre.jpg")
}

function setup() {
  createCanvas(1000, 600);

  noStroke();

// text
  textSize(28);
  text('Draw your own Danish pastries', 10, 30);

  fill(0, 102, 153);
  text('DIY_digital_kanelsnurre', 10, 60);
  fill(0, 102, 153, 140);
  text('DIY_digital_kanelsnurre', 10, 90);
  fill(0, 102, 153, 50);
  text('DIY_digital_kanelsnurre', 10, 120);

}
// view image
function draw() {
  // (x, y, width, height)
  image(img,700,10,267,200);

//
  if (mouseIsPressed) {
    // R, G, B - max. 255; use https://www.colorspire.com/rgb-color-wheel/
    fill(245,220,135);
    stroke(200,140,65);
  } else {
    noFill();
    noStroke();
  }
  ellipse(mouseX, mouseY, 110,110);
}
// future options for imporvement:
// press x to delete drawings;
// make the circular cursor go smaller when drawing;
// create a background that doesn't cover everything else
