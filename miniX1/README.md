## MiniX1: RunMe and ReadMe

[Click to view my project](https://annetietz.gitlab.io/aesthetic-programming-2021/miniX1/)

[Click to view my code](https://gitlab.com/annetietz/aesthetic-programming-2021/-/blob/master/miniX1/sketch.js)

![](screenshot.png)

_↑ Screenshot of my first program_


### Description of the program
For the very first miniX, I have created a RunMe that allows one to draw Danish pastries called "kanelsnurre" (which are very similar to cinnamon buns). At least that is my intention behind it. When the mouse is pressed, a circle appears that follows your motion, leaving behind a trace of circles from the position your mouse previously was at. By doing so, it creates the appearance of drawing. I got the idea while playing around with different types of ellipses, specifically in connection to the code used on [this page](https://p5js.org/get-started/). When using this code, with other colors at first, I liked the aesthetics I could create with it. 
The visuals reminded me of things one could see at a museum for contemporary art. At some point however, I started to see food in the drawings I was making, perhaps because I was hungry? Thus, after much trial and error, the idea came around. I added some text in the top left corner to give some sort of instruction and a picture of kanelsnurre I made last year in order to give some inspiration. 


### My learning experience
My first coding experience got me excited for what is to come. Taking simple steps at the beginning were somewhat easy to follow and the quick results were satisfying. However, as soon as things started to get a little more complicated, I can see how it is easy to get
discouraged. Talking with my group helped a great deal and I can see coding and programming as something collaborative, in contrast to the stereotypical antisocial loner that media likes to portray people with those skills as. Having something look and work as I anticipated is very rewarding to me. And even in the process, when things did not turn out as expected, I learned a thing or two along the way or even created something that gave me new ideas. As Bob Ross liked to say, "There are no mistakes, only happy accidents". I believe that even after just one week of reading literature, watching different content, talking and listening to lectures about coding, I already have a much more sufficient idea of what it is. The endless possibilities make me curious.


###  Coding and literacy
> “Literacy was not a neutral technology. As a tool for individual and social transformation it was always governed by purpose.” -Edward Stevens

Literacy equals power. In order to change the system, you need to understand the system. We no longer communicate through text only, but with an addition of code. "Coding for Everyone and the Legacy of Mass Literacy" by Annette Vee mentioned four main arguments for coding literacy, that came to be in correlation with Code.org’s “Hour of Coding” campaign, which were “individual empowerment; learning new ways to think; citizenship and collective progress; and employability and economic concerns”. So, in many ways comparing coding to the ability of reading and writing makes sense. There is however one point that I find differentiates the two from each other. My French teacher always said that “Sprache” (German for language) stems from “sprechen” (German for speaking). Especially when learning a new language, it is inevitable to make mistakes left and right. Some minor and some more major. But at some point, people will understand what you mean, even if your grammar is not quite right or you pronounce a word in a weird way. With coding on the other hand, every single part needs to be precise or else it will not be understood and thus run incorrectly. 

As stated above, the assigned reading gave me a new insight on how much power this new (new to me at least) form of reading and writing actually holds. It gives a good perspective on todays power dynamics and makes me think even more about the lack of diversity in this field. 
