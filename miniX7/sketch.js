let totoro;
let totoroPosX;
let totoroSize = {
  w: 100,
  h: 160
}

// min. amount of raindrops shown on the screen
let min_raindrops = 5;
let raindrops = [];
let score = 0, lose = 0;

// loading images
let img1;
let img2;

// reset button
let button;

function preload() {
  img1 = loadImage("MiniX7_sprite.png");
  img2 = loadImage("bg.GIF");
  bgmusic = loadSound("../assets/path_of_the_wind.mp3");
  myFont = loadFont("../assets/VanillaExtractRegular.ttf");
}

function setup() {
  createCanvas(640, 480);
  totoroPosX = width/2;
  textFont(myFont);
  // playing the sound
  bgmusic.play();
  bgmusic.setVolume(0.5)
  // looping the sound
  bgmusic.loop();
  // creating a reset button
  button = createButton("Start a new round");
  button.position(width - 130, 10);
  button.style("background","255");
  // reloading the canvas
  button.mousePressed(reloadCanvas);
  function reloadCanvas() {
    window.location.reload();
  }
}

function draw() {
  // insert img2 as background beneath
  background(img2);
  image(img1, totoroPosX, height - totoroSize.h, totoroSize.w, totoroSize.h);
  // checking the number of available raindrops
  checkRaindropsNum();
  // other functions
  showRaindrops();
  checkCatching();
  checkResult();
  displayScore();
}

// control functions
function keyPressed() {
  if (keyCode === LEFT_ARROW) {
    totoroPosX-=60;
  } else if (keyCode === RIGHT_ARROW) {
    totoroPosX+=60;
  }
  // reset if totoro moves out of the canvas
  if (totoroPosX > width) {
    totoroPosX = width - totoroSize.w;
  } else if (totoroPosX < 0 - totoroSize.w) {
    totoroPosX = 0;
  }
}

function checkRaindropsNum() {
  if (raindrops.length < min_raindrops) {
    raindrops.push(new Raindrops());
  }
}

function showRaindrops() {
  for (let i = 0; i < raindrops.length; i++) {
    raindrops[i].move();
    raindrops[i].show();
  }
}

function checkCatching() {
  // calculates the distance between each raindrop
  for (let i = 0; i < raindrops.length; i++) {
    let d = int(dist(totoroPosX+totoroSize.w/2, height - totoroSize.h/1.5, raindrops[i].pos.x, raindrops[i].pos.y));
    //close enough, as if catching the raindrop
    if (d < totoroSize.h/2.5) {
      score++;
      raindrops.splice(i,1);
    }
    //totoro missed the raindrop
    else if (raindrops[i].pos.y > height) {
      lose++;
      raindrops.splice(i,1);
    }
  }
}

function displayScore() {
    textSize(14);
    text("Controls: LEFT & RIGHT ARROW", 10, 20);
    text("Caught raindrops: "+ score, 10, 40);
    text("Missed raindrops: "+ lose, 10, 60);
}

function checkResult() {
  if (lose > score && lose > 2) {
    textSize(26);
    text("Too many raindrops missed", width/2 - width/3, height/2);
    noLoop();
  }
}
