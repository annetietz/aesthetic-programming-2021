## MiniX7: Games with objects

[Click to view my project](https://annetietz.gitlab.io/aesthetic-programming-2021/miniX7/)

[Click to view my code (sketch.js)](https://gitlab.com/annetietz/aesthetic-programming-2021/-/blob/master/miniX7/sketch.js)

[Click to view my code (Raindrops.js)](https://gitlab.com/annetietz/aesthetic-programming-2021/-/blob/master/miniX7/Raindrops.js)


### The process
This time around, I'm writing my Readme while working on the Runme again. I feel like it worked quite well previously. It's already Friday and I haven't started working on my code yet. If I'm being honest, I haven't put much thought into it, so that's probably why I haven't gotten an idea yet. Right now I'm leaning towards creating a game similar to "dress up games" making use of drag and drop. I have encountered numerus forms of games in my life and I'm still very fond of playing nowadays. To get a better understanding of it however, I looked up some definitions:

> "A game is a structured form of play, usually undertaken for entertainment or fun, and sometimes used as an educational tool."
> ([Wikipedia: Game](https://en.wikipedia.org/wiki/Game))


> "A video game is an electronic game that involves interaction with a user interface or input device – such as a joystick, controller, keyboard, or motion sensing device – to generate visual feedback for a player. This feedback is shown on a video display device, such as a TV set, monitor, touchscreen or virtual reality headset. Video games are often augmented with audio feedback delivered through speakers or headphones, and sometimes with other types of feedback, including haptic technology." ([Wikipedia: Video game](https://en.wikipedia.org/wiki/Video_game))

I want to add my own drawings for this miniX, as well as sounds or background music. I also looked at [this](https://itch.io/games/made-with-p5js) site for inspiration and to get an idea of what was possible when working with p5js. 

I've sketched the shelf that is in Anders' home. When we're sitting together as a group, I often tend to look at it and all the interesting trinkets it houses. An idea I had was to display the empty shelf and various items like books, plants or a lamp next to it, so the player could arrange them to their liking. 

![](shelf.jpg)

_↑ My first sketch_

I liked my initial thought, but felt like it wasn't "game-like" enough for me or at least didn't resemble the ones we've looked at this week. We've also played a lot of the old Crash Bandicoot series in our group, which made me feel somewhat nostalgic for these types of games (classic jump'n'run games). So, I came up with a different concept. Another thing close to my heart are the animated movies created by Studio Ghibli, which never fail to exude a miraculous atmosphere. They offer me a nostalgic form of escapism. I'd like to try to create a connection between the two for my game.

![](movie-poster.jpg)

_↑ Promotional poster from the Studio Ghibli movie "My neighbor Totoro"_


### Description of the game
![](screenshot.PNG)

_↑ Screenshot of the game depicturing one of the most iconic scenes of the movie_

The game I created ended up being somewhat similar to the "Tofu Go" inspired program we went through in class. At first I thought object oriented programming seemed quite easy and plausible, but when I started working with it I had some problems along the way. When making use of classes in particular, I feel like you really have to be focused and keep all the different elements of your code in mind. But I suppose that's something we will get used to in the long run when working with more complex/longer code. The player controls their sprite, which is portrayed by Totoro and his little umbrella, by using the left and right arrows on their keyboard. The goal is to catch as many raindrops falling from the sky as possible. The game is over as soon as more raindrops were missed than caught. I decided to not make the game too difficult, since it should be more of a relaxing and easy-going experience. It furthermore is not intended to be played for a longer period of time, as I imagine it to get boring pretty quickly. I initially wanted to add a starting screen showing a text of something along the lines of "press space to start the game", but due to some complications and time restrictions this syntax didn't make it into the final program. I also didn't use any 3D objects using WEBGL, which I would like to experiment with in the future. Besides that, some sort of instruction could've been nice for the person playing, maybe in correlation to a start screen. I will try to keep that in mind for next time.
Although working on this MiniX was harder than expected, I really liked engaging with games. But I wish we would've had more time to focused more on the creative and visual aspect rather than just trying to get it to work. 


### Programming objects and connection to a wider cultural context
Our course book mentions the following:

> "[...] the way we understand all media formats [...] is quite different from how a computer understand them as data, or - more precisely - as binary numbers." (Soon, Winnie & Cox, Geoff. Aesthetic Programming: A Handbook of Software Studies. London: Open Humanities Press, 2020., p. 145)

This phenomenon is interesting to me, because it seems like OOP is trying to bridge the way between the computer and the human way of thinking by describing attributes, properties etc. This can easily start a debate around the topic of generifcation and how it plays a role in our day to day lives. Are we not similar to computers after all? Trying to fit people and their characteristics into boxes, having prejudices and perpetuating stereotypes.


### Influence of Objected Oriented Programming
As I already mentioned, OOP makes sense to me, but can be quite hard to implement in the beginning. Our group also had an interesting discussion around OOP and OO (object oriented approach) in relation to how they're similar or different and whether they're obliged to coexist or could stand alone as well. In general, I would say that OOP is great for getting an overview and setting a plan for bigger projects, especially if different parties are involved. However, there's a possibility for not realizing possible potential that arises when "going with the flow" and operating more process driven. Both ways of doing things have their pros and cons.

![](MiniX7_sprite.png)


Music is from [here](https://www.youtube.com/watch?v=5EUChB9y5E0) ♪

Artwork by me

