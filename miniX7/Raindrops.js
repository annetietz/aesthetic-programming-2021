// classes are like cookie cutters, while the objects/instances are the cookies
class Raindrops {
  // constructor is like the object's setup()
  constructor() {
    this.speed = random(1, 2.8);
    //this.size = floor(random(20, 40));
    this.pos = new createVector(random(20, width), 6);
  }

  move() {
    this.pos.y += this.speed;
  }

  // apperance of the raindrops
  show() {
    ellipse(this.pos.x, this.pos.y, 4, 30)
    fill(250);
    noStroke();
  }
}
