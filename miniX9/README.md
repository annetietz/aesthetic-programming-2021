## MiniX9: Working with APIs (together with Jacob)

[Click to view our project "Made by ___?"](https://annetietz.gitlab.io/aesthetic-programming-2021/miniX9/)

[Click to view our code](https://gitlab.com/annetietz/aesthetic-programming-2021/-/blob/master/miniX9/sketch.js)


### Description of the program, conceptual thoughts and what APIs we’ve used
For this week’s MiniX, we got inspired by the discussions held during class in Aesthetic Programming and Softwarestudies, where we touched upon the topic of copyright issues and ownership. This was something that striked our interest and thus we wanted to further investigate the subject. It is something most of us have come across in some way or another, whether that is by having to check for copyright sensitive materials for school work or reading about big brands ripping off independent artists without permission or proper credit online. Especially in the digital age with the regular use of social media, not just by individuals but large corporations and brands as well, discussing these issues has become more relevant than ever. When examining the topic, we started asking ourselves questions like “When is something different enough from the source material to be considered original?” or “Is there even a way to universally set boundaries for that matter?”.
Our program consists of different smaller images essentially making up a "new" big image. 

![](Screenshot.png)
_↑ Screenshot of our program_


For our MiniX specifically, we were also influenced by the mass reposting of content (especially images) on various platforms on the internet. By the repeated process of taking a screenshot and posting it, taking a screenshot of the screenshot and posting it and so on, images lose their quality and it gets exponentially more difficult to backtrack the original source. This can for example be seen in memes, often taken from sites such as Reddit and then taken to Instagram, Twitter etc. It is almost as if many are of the impression that when something is posted to the Internet, it is free for use. When we’re out and about, we don’t take a bite of the food the stranger next to us is eating, just because we can, right?
Originality in itself can be a blurry concept, because it is hard to say where one should draw the line between blatant copying and creative interpretation. No matter how hard we try not to be, we will always (at least subconsciously) get influenced by whatever we’re consuming. So we’re asking again, how much does an image need to be altered for it to cross said line?


### Reflection upon our process and the significance of APIs in digital culture
> “Art historian and Warhol specialist Crone (1972) interprets Warhol’s use of silk screen as an implicit realisation of Benjamin’s claim that authors should not only feed the production apparatus with revolutionary content, but should also change the production apparatus itself. 
> The main aim is to make art more accessible, more democratic.”
> (Sollfrank Cornelia, "Warhol and his ‘Flowers’, The Conceptual Use of Silkscreen Technology”, PERFORMING THE PARADOXES OF INTELLECTUAL PROPERTY: A Practice-led Investigation into the Conflicting Relationship between Copyright and Art, The University of Dundee Duncan of Jordanstone College of Art and Design, 2011, pp. 260 - 261)


In this short excerpt, Sollfrank mentions Walter Benjamin’s take on mass production. Benjamin’s work was published in 1935 and concerns itself with the existence of art in our capitalist society and how the difference of classes at that time came into play. According to Benjamin, the reproduction of artworks generally leads to the loss of uniqueness and thus diminishes its authority, depending on what medium is used.
During the process of working with APIs, we indulged quite a bit in debugging. Something that especially stood out to us was an obstacle we came across when trying to view images from the Google API in fullsize instead of merely showing the thumbnail (smaller, compressed version). Here, we learned about CORS ([Cross-origin resource sharing](https://developer.mozilla.org/en-US/docs/Web/HTTP/CORS)). 


![](CORS.png)

_↑ A simplified model of how CORS works_
([Source](https://javascript.info/fetch-crossorigin))


Because of this, we had to find a way of changing the syntax to only search for non-copyrighted images, so we could display them in fullsize. We initially wanted to create a program that would pixelate the images using a slider like [here](https://editor.p5js.org/Andrew_Sink/sketches/YM-Ply_cD?fbclid=IwAR16yTzhszWaemhj0Vjh4tx__FYXeTSpFDQcr-vF7ZxoF-xTCtHDpiRpxao), but due the nature of APIs and only being able to fetch the image for a limited amount of times, we couldn’t put the necessary syntax in the `draw();` function. Finally, we ended up with a project displaying various images from the Google Image search, in a way creating a whole new image. For this we were inspired by mosaic-like pictures.


![](Mosaic.jpeg)

_↑ Example for our original inspiration_
([Source](https://www.ezmosaic.com/wp-content/uploads/2020/03/pmm-intro.jpg))


### Further thoughts on APIs
Dealing with owner- and censorship is a tricky topic, both in legal and in ethical terms. It’s interesting to us how APIs on the surface seem like a possibility to enable an open usage of data, but the more we looked into it the more hindrances we faced. In class we also learned that Google once had a limitless access to their APIs for non-profit organizations. However, this was removed a couple of years ago, probably because it didn’t create any economical profit. Of course, corporations like Google are technically allowed to do as they please (to a certain degree), but it makes you question their intentions and how they’re shaping the world we live in.
Besides that, if we had more time we would’ve liked to figure out a way to make our initial idea work, perhaps even with pictures from social media platforms. This would’ve probably illustrated our conceptual ideas more clearly, but we’re nonetheless satisfied with our work and are inspired by the thoughts we had and knowledge we gained along the way.


### Sources and further information
- Walter Benjamin, “Das Kunstwerk im Zeitalter seiner technischen Reproduzierbarkeit” (ENG: “The Work of Art in the Age of Mechanical Reproduction”)
- Sollfrank Cornelia, "Warhol and his ‘Flowers’, The Conceptual Use of Silkscreen Technology”, PERFORMING THE PARADOXES OF INTELLECTUAL PROPERTY: A Practice-led Investigation into the Conflicting Relationship between Copyright and Art, The University of Dundee Duncan of Jordanstone College of Art and Design, 2011
