let url = "https://www.googleapis.com/customsearch/v1?";
let apikey = "AIzaSyBAxJd37oHOvefC5lDBUp8KiE8EYGkmdxs";
let engineID = "61e9b7aa6a81ff371";
let query = "art";
let searchType = "image";
let imgSize = "medium";
let request;

let getImg;
let img;

function setup() {
    createCanvas(windowWidth,windowHeight);
    background(255);
	  fetchImage();
    pixelDensity(0.5);
}

function fetchImage() {
	request = url + "key=" + apikey + "&cx=" + engineID + "&imgSize=" + imgSize  + "&searchType=" + searchType + "&q=" + query;
	console.log(request);
	loadJSON(request, gotData);
}

function gotData(data) {
  let index = random([1, 2, 3, 4, 5, 6, 7, 8, 9]);
  console.log(index);
	getImg = data.items[index].image.thumbnailLink;
	console.log(getImg);
  img = createImg(getImg, "image", "", success);
  img.hide();
}

function success() {
  console.log("success");
  //image + status
  for (var i = 0; i <= 30000; i = i + 1){
    var xPos = random(-100, width);
    var yPos = random(-100, height);
    image(img, xPos, yPos, 30, 30);
  }
}
