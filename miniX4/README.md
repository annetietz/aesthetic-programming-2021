## MiniX4: Capture All

[Click to view my project](https://annetietz.gitlab.io/aesthetic-programming-2021/miniX4/) (Program works best if the user is close to the camera)

[Click to view my code](https://gitlab.com/annetietz/aesthetic-programming-2021/-/blob/master/miniX4/sketch.js)


### Transmediale submission: Sell your soul for cookies
Everyone knows the pop ups that appear either at the top, bottom or right in the middle of a website asking you for permission to use cookies. There's a variety of options to choose from, depending on the website. Some show the option to accept all cookies in form of a big, green button, while the option to decline is in a small, gray box below. Similar to the "I have read the terms and conditions" checkbox, we don't really pay attention to the use of cookies anymore either. We simply got used to it over time, that we pay it no mind by now. So, when a website or a service would ask for something else than the usual, something we might not want to agree to, we probably would overlook it. We have become numb to the idea of big corporations taking and using or data. We actively have to make a decision each time we enter a new website, use a new service and so on. But we're lazy and have other things to worry about. The program I created shows the following pop up, but only when you blink:

![](cookies.PNG)

_↑ The "pop up" shown on the screen_

The fact that the window only appears when the eyes are closed is a conscious choice meant to criticize not only the user's behaviour, but also the industry concealing the quantification of human life for (mostly) economic value. At the same time, I don't know an exact solution for this issue. I hope this piece can stimulate discussion around the topic and remind us of how our data is constantly being captured, even when we sleep.

### Description and process of the program
As mentioned above, the program uses face tracking (clmtrackr) to recognize when the eyes are blinking (closed). I found a code that tracks the users blinking [here](https://editor.p5js.org/2sman/sketches/b-KAoyXTF). My main motive with this program was to bring attention to the ignorance we bring to the table when facing data policies. It was fun to play around with face recognition and testing the boundaries of it. My group and I also worked with voice recognition and the tracking of hand movements using [this](https://handsfree.js.org/?fbclid=IwAR1N2btmzgFwz1tXptysTeep6XyI2zD8WeZF2XD1ZnuRBIUyesB0bgAdU_A#installing). I initially wanted to use filters, such as `filter(GRAY);`, but found out that it sort of slowed down the video/made it lag. I also concluded that the face tracking isn't that reliable and sometimes glitches and doesn't work as well as I would've liked it to. If I'm being honest, I'm not 100% content with my program for this miniX, but I'm excited about the new features we have learned about. 

### The theme of "capture all and cultural implications of data capture"
This time around, I really enjoyed the assigned reading. It gave me lots of of insight on the issue of data capture. I wrote down the following notes, that influenced my final program and guided me throughout my thought process:

- Quantification of human life through digital information for economic value

- Data does not naturally exist, but emerges through a process of abstraction

- Datafication becoming "an accepted new paradigm for understanding social behaviour"

- Appropriation of social resources

Regarding cookie laws in particular, I found out that 
> all websites owned in the EU or targeted towards EU citizens, are now [as of May 2011] expected to comply with the law." (source: [cookielaw.org](https://www.cookielaw.org/the-cookie-law/))

The more I learn about programming, digital culture and the ethics behind them, the more I realize how greatly those matters impact our daily lifes and way of life as well. Ignorance leads to exploitation and unspoken rules. I believe that I and most people around me are a victim of datafication. We talked about this in my group and opinions varied, which I found interesting. Some believe that it is perfectly acceptable for tech-giants such as Google to use and sell our data, since we are using their services in return. It is the currency we provide. Others however were more so concerned with privacy issues and the untransparent composition of it all. I can understand both sides and don't really know where I stand myself, but I can definitely say that for the most part, it is the easier decision to simply ignore the infiltration of our private space and accept that this is how it's going to be. If that's the right way and if there even is such thing as a right way remains an unsolved question and is up to debate. A debate I would like to start within myself and with others.
