let capture;
let tracker;
let positions;
let leftwinks = 0;
let img;

function preload() {
  img = loadImage("cookies_mirrored.png");
}

function setup() {
  createCanvas(640, 480);
  //webcam capture
  capture = createCapture(VIDEO);
  capture.size(640, 480);
  capture.hide();
  //setup face tracker/load clmtrackr
  tracker = new clm.tracker();
  tracker.init();
  tracker.start(capture.elt);
}

function draw() {
  //flipping the video (mirrored)
  push();
  translate(width,0);
  scale(-1, 1);
  //draw the captured video on a screen with the image filter
  image(capture, 0, 0, 640, 480);
  pop();
  //flipping the canvas (mirrored)
  translate(width, 0);
  scale(-1.0, 1.0);
  //updates the tracker with current positions
  let positions = tracker.getCurrentPosition();

  if (positions.length > 0) {
    if (dist(positions[26][0], positions[26][1], positions[24][0], positions[24][1]) < 18) {
      print("you winked your right eye");
      image(img, random(0, 640), random(0, 480), 300, 100);
    }
    if (dist(positions[29][0], positions[29][1], positions[31][0], positions[31][1]) < 18) {
      print("you winked your left eye");
      image(img, random(0, 640), random(0, 480), 300, 100);
      leftwinks++;
      print("left winks: "+leftwinks);
    }
  }
}
