## MiniX3: Designing a throbber

[Click to view my project](https://annetietz.gitlab.io/aesthetic-programming-2021/miniX3/) (If sound isn't playing, check if audio is allowed to play for the site)

[Click to view my code](https://gitlab.com/annetietz/aesthetic-programming-2021/-/blob/master/miniX3/sketch.js)

### Description of the program
The throbber I made is not a single icon, but rather something that takes up the whole screen. Once the page is opened, rain starts to appear on a dark grey background accompanied by the sound of it. For me, rain gives off a strange feeling of time standing still, while simultaniously going by fast without even noticing it. Both rain itself and the sound of it feels very calming and gives me a sense of comfort. Many other throbbers we encounter regularly trigger aggressive or annoyed feelings in us (e.g. Apple's "Spinning wheel of death"). With my throbber, I wanted to give a relaxing and pleasing alternative to the more common throbbers. I was somewhat inspired by the Dinosaur game the Google Chrome browser runs, when there is no internet connection. It transforms a waiting situation into a fun experience and gives us a sense of control. With that being said, I can also see a problem in how our society is always looking for something to do due to constant overstimulation, especially with digital media. Sometimes it's nice to just stop and enjoy the rain for a while.

![](dinosaurgame.jpg)

_↑ Google Chromse dinosaur game_

![](Screenshot.PNG)

_↑ My program_

### What did I explore and what did I actually end up using?
The idea came to me quite quickly this time, but (as aspected) I had some problems with making it work how I wanted it to. This was mostly due to using different types of loops, which I had no prior experience in. In the beginning I was able to make "animated lines", but I had trouble using the `translate();` function. I wanted to reposition the 0-point of the x-achis for every new line created, but instead of it being executed once for every line it just made them move from left to right nonstop. I probably spent the most time yet on this program and eventhough most of it didn't make it into my final code, I believe it was worth exploring different ways. Another thing I know now more about is using sounds, which I would like to use in future projects as well. For the sound, I actually created a button that let you turn it on or off. But I liked it more the way it was without. It felt closer to reality in a away, since we can't control the weather to our liking either. This time around, I got help from my group members again for some things, which I was grateful for. My code ended up being quite short and easy, but I don't think that that's necessarily a bad thing.


### Reflection upon throbbers and temporality in general in digital culture
After our aesthetic programming and softwarestudies class, I realized that throbbers are often used to conceal certain transactions or processes, rather than making them more transparent for users. Yet another way of tricking the consumer and enlarging the gap between what we think is happening and what is actually happening. 

> "Technology has been a tool through which humans create distance from natural cycles and design their own time experiences." -Hans Lammerant, "How humans and machines negotiate experience of time," in The Techno-Galactic Guide to Software Observation, p.96, (2018)

Like many aspects of our lives, time in the "real world" seems to be a social construct. It keeps us sane, gives us something to rely on and is something we collectively choose to believe in. We simply copy this construct onto computation to give correlation. 
Honestly, thinking about this too much gives me a headache, so I will stop here.

