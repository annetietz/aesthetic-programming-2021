// variables -> declaring and initializing
// variables for the lines
let x1 = 0
let y1 = 0
let x2 = 0
let y2 = 0

// initializing the constant for the range of the drops
const droprange = 2000

// loading the sound
let rainSound;
function preload() {
  soundFormats("mp3", "ogg");
  rainSound = loadSound("../assets/rain.mp3");
}

// "setup" happens just once
function setup() {
  // creating and setting the size of the canvas
  createCanvas(windowWidth, 600);
  // background colour
  background(50);

  // looping the sound
  rainSound.loop();
}

// "draw" loops over and over
function draw() {
  // luminosity and alpha of the background
  background(50, 30);

  // creating and looping the rain
  for(let counter=0; counter<100; counter++) {
      // colour of the rain
      stroke(200);
      // using the constant for the range of the drops
      x1 = (random(-droprange, droprange))
      y1 = (random(-droprange, droprange))
      x2 = (random(-droprange, droprange))
      y2 = (random(-droprange, droprange))

      // creating the line(s) used for the rain
      line(x1, y1, x1, y2)
  }
}
