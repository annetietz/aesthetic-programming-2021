## MiniX6: Revisit the past

[Click to view my project](https://annetietz.gitlab.io/aesthetic-programming-2021/miniX6/)

[Click to view my code](https://gitlab.com/annetietz/aesthetic-programming-2021/-/blob/master/miniX6/sketch.js)


### What does aesthetic programming mean (to me)?
Up until now I've always written most of my Readmes after being finished with the Runmes. This time around I want to start putting down my thoughts while working on my program upon recommendation from one of our instructors. Before choosing which MiniX to revisit, I would like to redefine what aesthetic programming means to me at this point in time, now that I have indulged in the methodology of "Critical Making" and having read the preface of our course book. Critical making very much focusses on the process, rather than the finished product. It's about asking questions prior to and during the process, as well as considering different paths instead of going with the usual or easiest route. 

> "Ultimately, critical making is about turning the relationship between technology and society from a "matter of fact" into a "matter of concern"." (Ratto, Matt & Hertz, Garnet, "The Critical Makers Reader: (Un)Learning Technology", p. 20)

Aesthetic programming is concerned with questioning existing technological paradigms and striving for change, making it similar or correlating to critical making. Instead of seeing coding as merely functional, it can be viewed as a creative and thought-provoking outlet enabling critical practices in the arts and humanities. 


### Description of the program and its changes
We talked about how to tackle this week's exercise in our instructor class. My group member Jacob had an interesting perspective. He suggested to use critical making to criticise one's own process (and result) instead of making a brand new miniX, which gave me a better understanding of how to use it in this case. When reading the objective for miniX6, I found it particularly eye opening to try taking the recieved feedback into consideration and thus see it through someone else's eyes. I decided to rework my miniX5, which was dealing with the topic of generative programs. The feedback I got here was mentioning how the program didn't really end up being generative, which was something I was concerned about when handing it in as well. Besides that, I believe that I was more so focussed on the visual aspect, rather than the conceptual or technical one. The conceptual part wasn't as well thought out as I normally would want it to be. That alone is enough reason for me to revisit it already. Adding on to this, as mentioned in the feedback, it was hard to see the generative element for others, so I feel like I didn't think of that enough, which is something I really want to change for my reworked miniX.

![](Screenshot1.png)

↑ _My previous program_

![](Screenshot2.PNG)

↑ _My new program_

I reworked the code, keeping some elements, like the circular shapes and colorways. This time, I used `noise();`, which helped to balance the visuals and make the changing colors seem more natural and alive. My group member Jakob used this syntax as well and helped me to get it right. Each time the program is loaded it creates a unique sculpture-like piece making it truly generative.
Now for the conceptural part. The 8th of march celebrated women's day. However after that followed countless upsetting acts that make me question its sincerity and humanity in general more and more, such as hashtags like #notallmen, the discussion of the "super-straight sexuality" justifying transphobia and racially motivated hate crimes against asian women in the US leading to murder (that were verbally defended by the policemen in charge).

![](Image-1.jpg)

I was deeply affected by these events on a personal level feeling somewhat suffocated and unable to help. Although it is not much, I want to dedicate this program to all women/womxn enduring so much ignorance and pain day after day. The pastel colurs might act as an escape from this harsh world by creating a comforting and pleasing moment to oneself.
I can tell now that I put much more thought into this miniX, not only into the finished product but also into the deicisions made throughout. This resulted in me being more content with my program, although there still are some minor issues I would've liked to be improved. I know that I didn't exclusively think about myself, but others too. This ultimately leads to me getting a broader perspective and learning experience out of the process.

### Programming as a practice/method for design and its relation to digital culture
As mentioned previously, programming can of course be used for practical matters, but is much more than just that. It can be a form of creative outlet, acting as a tool - just like paper and paint for an artist or the voice and instruments for a musican. Like every medium, there's both benefits and downsides to it. Relating it to digital culture can be interesting, because it enables us to deconstruct already existing schemes. For me, "looking behind the scenes" (understanding the code and inentions behind it) allows me to see all things digital from a whole nother perspective. It's somewhat empowering while simultaniously showing me the bad (or better put biased) intentions of others that I, as a user, might not have thought about before. So in other words, programming lets us create, question and indulge more in already existing substance, as well as making us aware about the things we don't know.
