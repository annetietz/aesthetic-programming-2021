// variable for the gradient circles
let dim;
// variables for the bouncing balls
let y = 0;
let speed = 4;
let gravity = 0.2;
// perlin noise color
let xoff = 0;
// perlin noise ball x-poisiton
let xoff1 = 0;
// loading the font
let myFont;
function preload() {
  myFont = loadFont("../assets/arial.ttf");
}

function setup() {
  background("#4c0f05");
  createCanvas(windowWidth, windowHeight);
  // choosing the size of the cirles
  dim = width / 3;
  // setting the colorMode to HSB (instead of RGB for example), a little bit like the color wheel (min. 0, max. 360)
  // hue value, saturation, brightness
  // setting the color range
  colorMode(HSB, 360, 360, 100);
  noStroke();
  ellipseMode(RADIUS);
}

function draw() {
  for (let x = 0; x <= width; x += dim) {
    // gradient part
    drawGradient(x, height / 2);
  }
  // perlin noise ball x-position
  xoff1 = xoff1 + 0.01;
  let n = noise(xoff1) * width;

  // drawing the balls
  ellipse(n, y, 26, 26);
  y = y + speed;
  speed = speed + gravity;

  // slowing down the speed
  if(y > windowHeight) {
  // reversing the speed
  speed = -0.92 * speed;
  }
  // text
  push();
  textSize(50);
  textFont(myFont);
  fill(360);
  text("♀", windowWidth / 2, windowHeight / 2);
  pop();
}

function drawGradient(x, y) {
  // choosing a random color from the whole color wheel as the hue value
  let pnoise = map(noise(xoff), 0, 1, 0, 360);
  // hue value, saturation, brightness
  fill(pnoise, 90, 90);
  // perlin noise speed
  xoff += 0.0045;
}
