## MiniX5: A generative program

[Click to view my project](https://annetietz.gitlab.io/aesthetic-programming-2021/miniX5/) (EPILEPSY WARNING)

[Click to view my code](https://gitlab.com/annetietz/aesthetic-programming-2021/-/blob/master/miniX5/sketch.js)


### The rules and performance over time of the program
The program I made for this miniX consists of cricles with a gradient effect changing colors. They are accompanied by three smaller circles that move up and downwards, creaing the illusion of gravity.

The rules I set for this program were:
1. Make shapes bounce up and down, simulating gravity
2. Randomly change colors in a set range

![](Screenshot.PNG)

_↑ The three small balls are bouncing up and down when running the program_

In terms of emergent behavior, the rules change the overall apperance of the program. This could have been made a little more interesting by enlarging the range of colors used, but I enjoyed the pastel theme and decided to go with that.

As far was my work process goes, I wasn't able to figure out how to isolate the `frameRate();`, which I wanted to use to make the colors change slower while still maintaining a smooth animation of the moving elements. Other than that, I didn't have too many problems this time around. However, after running the program for a little while, the balls keep jumping up and down. It would have been nice to implement a function to stop them from doing so and making it more realistic.
I was inspired by the work of [Sol LeWitt](https://github.com/wholepixel/solving-sol), which made me go down a more artistic route. To understand how to implement the bouncing effect I watched a [Daniel Shiffman video](https://www.youtube.com/watch?v=YIKRXl3wH8Y), which nicely explained the concept of it. I got the code for the gradient from [here](https://p5js.org/examples/color-radial-gradient.html) and tinkered around with it to understand what the different parameters are used for. Granted, it's unfortunately not that easy to see its full effect, because of the high frame rate.


### The role of rules in the program
As mentioned above, the rules contribute to the visual experience, as well as creaing a connection to reality by mimicing gravity. When looking at the program for a longer period of time however, one can see that the balls never stop moving, thus distancing themselves from reality after all.


### The idea of "auto-generator"
I personally wouldn't say that the MiniX contributed a whole lot to my understanding of "auto-generator", however talking to my group members about the topic, as well as having in-class discussions certainly did. Langton's Ant was really interesting to me. It seems simple on the surface and isn't too complicated code wise, but was able to spark a conversation (maybe exactly because of its simplicity). I especially enjoyed comparing the mechanics to nature. In regards to setting rules and thus limitations, it is fascinating how one can give the exact same rules, but different people, machines and so on will interprete it uniquely.
