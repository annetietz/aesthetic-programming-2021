// variable for the gradient circles
let dim;
// variables for the bouncing balls
let y = 0;
let speed = 4;
let gravity = 0.2;

function setup() {
  createCanvas(600, 400);
  // choosing the size of the cirles
  dim = width / 6;
  background(0);
  // setting the colorMode to HSB (instead of RGB for example), a little bit like the color wheel (min. 0, max. 360)
  // hue value, saturation, brightness
  // setting the color range
  colorMode(HSB, 360, 360, 100);
  noStroke();
  ellipseMode(RADIUS);
  // how many times the visual changes per sec (optional)
  // frameRate(1);
}

function draw() {
  background("#feeff1");
  for (let x = 0; x <= width; x += dim) {
    // placement of the gradient part
    drawGradient(x, height / 2);
  }
  // drawing the bouncing balls
  ellipse(100, y, 20, 20);
  ellipse(300, y, 20, 20);
  ellipse(500, y, 20, 20);
  y = y + speed;
  speed = speed + gravity;

  // slowing down the speed
  if(y > 400){
  //reverse the speed
    speed = -0.92 * speed;
  }
}

function drawGradient(x, y) {
  let radius = dim / 2;
  // choosing a random color from the whole color wheel as the hue value
  let h = random(0, 360);
  for (let r = radius; r > 0; --r) {
    // hue value, saturation, brightness
    fill(h, 90, 90);
    // drawing the ellipse with variables
    ellipse(x, y, r, r);
    // apperance of the gradient
    h = (h + 0.5) % 360;
  }
}
