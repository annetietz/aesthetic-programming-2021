## MiniX2: Geometric emoji

[Click to view my project](https://annetietz.gitlab.io/aesthetic-programming-2021/miniX2/)

[Click to view my code](https://gitlab.com/annetietz/aesthetic-programming-2021/-/blob/master/miniX2/sketch.js)


### Description of the program
For this weeks miniX, I created a program that allows the user to visualize their emotions through colours. It is directed towards the expression of emotions mostly seen as "negative" - envy, anger and sadness. Here, envy is represented by the colour green, anger by red and sadness by blue. The user then can regulate the amount of colour mixed into the background by adjusting the sliders to their liking, depending on how they are currently feeling. 
It is often harder to share said feelings with others or put them into words, than it is with "positive" ones. We are much more eager to tell friends when we are feeling on top of the world, rather than stuck in a slump. 
I decided to go against the typical emoji setup of two big eyes, a round face and an expressive mouth. Two circles were however added in order to make the space easier recognizable as a face. Although I am not sure whether I prefer it with or without the eyes.

![](1.PNG)
![](2.PNG)

_↑ Different iterations of the program with or without eyes_

The space where a mouth normally would be is covered by a blooming flower that represents how all emotions are part of growth, which makes us human. Flowers are usually associated with beauty and all things likeable. When using the program myself, I also discovered that sometimes a combination of colours would end up generating a pleasing pastel colour, that further supports the stated argument and contrasts the way in which "negative" emotions are unwelcome. How can a combination of sadness and anger for example be something beautiful? It is all a sense of perspective I suppose.

![](3.PNG)
![](4.PNG)
![](5.PNG)

_↑ Screenshots of the final program_


### What have I used and learned?
This time around, I tried to come up with some ideas before starting to work on my code. Just like last time however, I quickly realized there were a lot of things I wanted to do, but was not yet able to. This got me discouraged quickly. So for the future I want to keep in mind that it is better to have a rough idea and be open to changes instead of having a detailed plan, only to then realize it either takes too much time or is not doable at this state.
I first set up my canvas as usual, using `createCanvas()`. Another element I already was familiar with, was inserting images and positioning them using `var img`,`function preload()` and `img.position()`. Next up was sliders, which was new for me. It caused me some problems, but I eventually found a variable on the [p5.js reference page](https://p5js.org/reference/), that worked out for me. To be honest there are a few lines I don't understand 100%, but I'm a lot more comfortable with it now after having played around with different options. The eyes were easy to add as well, by using `circle()`. This time I played around with the opacity, making the eyes a little see-through. I was however not able to set things like `stroke()` or `fill()` for the circles only. Everytime I changed these elements, the text next to the sliders was affected as well. And lastly, there actually is the option to save the canvas by pressing the "s" button on the keyboard. But it only saves the background, eyes and text, not the flower included. I was looking into only saving a certain part of the canvas, but ended up defeated and confused. Before ending up with my current program I also wanted to run things such as a gradient background, "animations" using WEBGL, placing elements outside the canvas and making buttons that actually do something. My group was able to help me a great deal, but there is still a lot that did not turn out how I wanted it to. This feeling is probably something I will have to get used to.


### Emojis in a wider social and cultural context
Discussing with my peers led me to the conclusion that emojis nowadays can add personality to an otherwise bland text message. It is almost rude to not use them in some contexts. 
On the other hand, many resort to letting emojis speak for themselves, thus leaving room for misinterpretation and perhaps confusion on the receiving end. Instead of openly describing the emotions one might be going through, it is much easier to send an emoji that seems to somewhat represent one's inner world. There are so many to choose from after all. Of course, we can hide behind words as well, but it might take a little more effort than just typing ":)" or ":/" and hitting send.
But, we have to remember that emojis are much more complex that a yellow circle with some black lines and dots. They have evolved from simply emulating facial expressions to being a part of texting norm and something users identify themselves with. Emojis can mean different things than what they were intended to. Just like emotions, people are complex. 

> "[...], the problem of universality begins with the assumption that anything can should be encoded in symbolic logic." (Roel Roscam Abbing, Peggy Pierrot and Femke Snelting, "Modifying the Universal." Executing Practices, Eds. Helen Pritchard, Eric Snodgrass & Magda Tyżlik-Carver, London: Open Humanities Press, 2018)

Emojis have over time become more and more realistic leading to people eihter being misrepresented with stereotypical assets or not being represented at all. But is it even possible and if so how, to change something that is so integrated into everyday culture for most of us? Will there be more and more variations of emojis or will they be changed all together? Only time will tell. Until then, we should do our best to stay critical and support those who are left out.
