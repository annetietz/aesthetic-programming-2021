// image
var gif_createImage;

function preload() {
  gif_createImg = createImg("flower.gif");
}

// sliders
let rSlider, gSlider, bSlider;


function setup() {
  // create canvas
  createCanvas(650, 550);

  // create sliders
  gSlider = createSlider(0, 255, 0);
  gSlider.position(20, 20);
  rSlider = createSlider(0, 255, 100);
  rSlider.position(20, 50);
  bSlider = createSlider(0, 255, 255);
  bSlider.position(20, 80);
}


function draw() {
  // placement of image
  gif_createImg.position(125, 260);

  // sliders
  const g = gSlider.value();
  const r = rSlider.value();
  const b = bSlider.value();
  background(r, g, b);
  text("ENVY", gSlider.x * 2 + gSlider.width, 35);
  text("ANGER", rSlider.x * 2 + rSlider.width, 65);
  text("SADNESS", bSlider.x * 2 + bSlider.width, 95);

  // drawing the eyes
  circle(350, 210, 40);
  circle(250, 210, 40);
  fill(0, 0, 0, 150);
}

// press key "s" to save canvas
function keyPressed(){
  if(key == "s"){
  save("myEmotion.png");
  }
}
