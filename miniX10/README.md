## MiniX10: Flowcharts

### Group work
![](Screenshot2.png)

_↑ Flowchart for idea 1 "Porn bots"_

![](Screenshot1.png)

_↑ Flowchart for idea 2 "Game of Capitalism"_


### Description of our flowcharts
#### "Porn bots" idea:

The idea for our first flowchart was inspired by spam culture, in particular very sexual spam messages received on social media platforms such as Instagram or Facebook. These kinds of messages usually use an image of a woman wearing close to nothing on their profile picture and try to lure you into clicking on some dubious link. The structure is often reminiscent of one another, so our plan would be to divide them into four parts. The program would then take lines from different messages and thereby create a new one. The fact that the messages never truly stop and keep generating is a reflection on the reality of spam culture. Conceptually, we thought a lot about the hypersexualization of women, identity theft and spam bots as a form of black box.

#### "Game of Capitalism" idea:

Our second flow chart focuses on a critical commentary on our capitalistic society. It’s loosely inspired by Conway’s “Game of Life”. This was especially interesting, because opinions among our group are quite divided on the topic. When starting the game, both small and big circles would appear and move around. Once a big circle touches a smaller one, the smaller one would be consumed. The player also has the option to create circles themself, thus adding onto the game. The program touches upon privileges certain groups have and the false belief of everyone having the same chance (to for example obtain economic wealth). In the end there would be one circle who consumed all others, fully taking up the whole screen. Is this how our future will be shaped, through a monopoly of one giant company having all the power and others not being able to establish themselves independently? Here, we were thinking about how far our traits are animalistic and the concept of survival of the fittest.


### Difficulties in relation to keeping things simple at a communication level whilst maintaining complexity at the algorithmic procedural level 
The difficulties involved in trying to keep things simple is that the sketches of the programs for the final project right now are very rough. In order to specify the algorithmic flows of the program, you need to have some sort of consensus about the program's syntactical structures and the process. On the other hand, once the programs have a clear syntactical structure, it makes it easier to chunk it down into smaller sequential steps. The next problem is addressing the flow of the program in a way that is understandable to an audience without programming experience, while at the same time encapsulating the characteristics of the code’s performance in the program. This can also be linked to the issue concerning transparency in algorithmic procedures - and the notion of the so-called “black box”. Cox & Soon write:


> It is clear that algorithmic procedures play an important role in organizing culture, and subjectivities, and it is not very easy to see through or describe them because they operate beyond what we experience directly. They produce wider effects in the ordering of life. Algorithms do things in the world and have real effects on machines and humans. 
(Cox, Soon, p. 220)

As mentioned here, the issue is the effects algorithms have on our lives, while maintaining a distance or “operate beyond what we experience directly” (Soon, Cox, p. 220). This notion of abstraction complicates our understanding of algorithms on a communicational level - this is also due to their somewhat pervasive, yet essential, influence on our culture and society. It also maintains the distinction between “normal people” and the idea of programmers as “sorcerers” producing somewhat “magical” artefacts and algorithms (Bucher, p. 19).
Another issue is also the ambiguity a program can have in regards of usefulness, interaction possibilities and conceptual linkage. It is dependent on the person using it, the hour, the situation around the individual using it and so on - in other words it is situated. A flowchart is therefore only “one way” of looking at/describing the program - and it often says more about the bias of the person creating the flowchart than about the nature of the program being described. In this matter it is a subjective description of the program and a prescription of how you are “supposed” to interact with it.


### Technical challenges we might face and how we want to address them
The two programs are in complexity fairly similar, they both have a generative nature and rely on user interactions. The syntax however varies a lot between the programs. “Porn bots” will mainly work with JSON-files and APIs in order to generate bot-like messages which the user can interact with. The technical challenge in relation to this program will be to implement an API that generates profile pictures from a source like [thispersondoesnotexist.com](https://thispersondoesnotexist.com/). Other than technical challenges, there will also be some logistic ones in terms of gathering enough spam messages to truly accomplish a generative nature of the program and not be too repetitive.
“Game of Capitalism” will mainly consist of objects and loops in order to create a game that is in some way random but always ends up with the same result. The technical challenge here will be to create an algorithm that favortises the biggest circle and makes it chase down the smaller circles.
We will address these challenges by researching the API and the circle syntax in order to understand the syntax and how it could be translated into our program. 


### How the flowcharts are useful to us
Flowcharts can be a useful tool for understanding computer based programs because they visualise how a program works in a computational sense but also the conceptual thoughts behind the program. Often computer based programs can be complex and thus difficult to understand for people who did not create them or have extensive knowledge about programming. It can be equally challenging for creators to explain it to others without simplifying too much or not simplifying enough as well. Flowcharts can help to envisage individual ideas to others in a quick and easy way, making it useful for group projects where people come from different backgrounds. When creating a flowchart it’s necessary to divide whatever is being described into individual parts. Thinking it through can help to identify possible hurdles one might face beforehand. The communication part is a crucial benefit and helps to get everyone on the same page in terms of conceptual thoughts and general direction.
When creating the flowcharts for both of our ideas, we were intrigued by thinking about whether our programs have an actual end to them or just go on forever, making us consider these factors more. This time around we used the flowchart more as a starting point rather than something finite. So in reality, we probably wouldn’t exactly stick to it, but use it more as a guide and a tool to get a mutual understanding of what we want to create and where the individual steps should lead us.


### Individual work
![](Screenshot3.jpg)

_↑ Flowchart for MiniX7: Game with objects (Totoro game)_


### Individual reflection
For the individual flowchart we had to revisit our most complex MiniX so far. I chose "MiniX7: Games with objects" to illustrate as I find its code was very challenging for me. For our group flowcharts, we took on the perspective of a user of the program, thus focussing on what will be shown instead of technical details. When working on my own, I went through the code from start to finish, trying to emulate the way the program is read by the computer. I'm not sure if this is the best way to do it, but it gave me some form of structure and a way to check if there was something I might have missed through a different approach.
In this flowchart, I narrowed certain things down, such as `bgmusic.play();`, `bgmusic.setVolume(0.5)` and `bgmusic.loop();` to simply "playing the music". Other things I didn't visualize in my flowchart were how the direction of the movement of both the Totoro sprite and the raindrops was implemented or the way I made use of OOP through classes. Although some of the syntax in a way gets lost by doing so, it's necessary to simplify things to a certain degree. How far this simplification goes is up to the person making the flowchart, in this case me.
Revisiting and trying to think about how I would boil down the game in order to explain it to someone else and describe the process without using terms specific to coding languages actually made me understand the program better too. This is of course interesting, because you would expect the person who made the game to fully have gotten their head around it but that's not always the case. 
