// "Transcending the b1nary" by Anders & Anne

let level1;
let level2;
let level3;
let level4;
let level5;

let levelA;
let levelB;
let levelC;
let levelD;

let myFont;

function preload() {
  // different json files
  level1 = loadJSON("level1.json");
  level2 = loadJSON("level2.json");
  level3 = loadJSON("level3.json");
  level4 = loadJSON("level4.json");
  level5 = loadJSON("level5.json");

  levelA = loadJSON("personality_test.json");
  levelB = loadJSON("genders.json");
  levelC = loadJSON("mass_surveillance_project_names.json");
  levelD = loadJSON("academic_subjects.json");

  // font
  myFont = loadFont("../assets/AndaleMono.ttf");
}

function setup() {
  createCanvas(windowWidth, windowHeight);
  background(255, 200, 140);

  let levelI = level1.levelone;
  let levelII = level2.leveltwo;
  let levelIII = level3.levelthree;
  let levelIV = level4.levelfour;
  let levelV = level5.levelfive;

  let lvlA = levelA.personality_test2;
  let lvlB = levelB.genders2;
  let lvlC = levelC.projects;
  let lvlD = levelD.subjects;

  // title
  push();
  textFont(myFont);
  textSize(20);
  text("Transcending the b1nary", 100, 80);
  pop();

  // poem
  let xPos = 100;
  let yPos = 70;

  textFont(myFont);
  textSize(12);

  text(random(levelI), xPos, yPos + 60, width-200, 100);
  text(random(levelII), xPos, yPos + 100, width-200, 100);
  text(random(levelIII), xPos, yPos + 140, width-200, 100);
  text(random(levelIV), xPos, yPos + 180, width-200, 100);
  text(random(levelV), xPos, yPos + 220, width-200, 100);

  let xPos2 = windowWidth / 2;
  let yPos2 = 400;

  text(random(lvlA), xPos2, yPos2 + 60, width-200, 100);
  text(random(lvlB), xPos2, yPos2 + 100, width-200, 100);
  text(random(lvlC), xPos2, yPos2 + 140, width-200, 100);
  text(random(lvlD), xPos2, yPos2 + 180, width-200, 100);
}
