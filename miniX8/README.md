## MiniX8: E-lit (together with Anders)

[Click to view our project](https://annetietz.gitlab.io/aesthetic-programming-2021/miniX8/) (Works best in fullscreen)

[Click to view our code](https://gitlab.com/annetietz/aesthetic-programming-2021/-/blob/master/miniX8/sketch.js)


### Transcending the b1nary
Our e-literature piece “Transcending the b1nary” focuses on the absurdity that politics present to us in this day and age in terms of gender inequality and loss of freedom of information. We also focus on questioning what right or wrong, true or false and moral or immoral is. The first line of the poem starts out with a specific statement concerned with a recent issue and then goes on to widen its perspective further and further up until the last line, essentially spiraling into meaninglessness and inquiring existence itself. The poem deals with the growing skepticism seen in Europe about gender and feminism on an academic level. The poem is therefore based on quotes from political opponents of gender studies and feminist approaches to academia. The problem is that in some places, governments have begun to legislate and control people's freedom of research. By removing the possibility of research, one also removes people's freedom of information. The consequences of this will be great and create an even larger inequality which further decreases the possibilities of minorities. Just as surveillance is meant to restrict and shape us, limitation of access to certain information and education tries to alter our understanding of reality into what is beneficial for the ones implementing the constraints. By narrowing people's ability to access information, a reality is created where people no longer have the opportunity to live their lives freely.

![](Screenshot.png)

_↑ Screenshot of our poem "Transcending the b1nary"_


### Process and what we’ve learned along the way
This time around we were introduced to working with data, in particular json-files. We created five different json-files for each level that contained a number of quotes needed for our e-lit piece. We furthermore made use of datasets from json-files from [here](https://github.com/dariusk/corpora/tree/master/data), that dealt with topics complementing our initial poem, creating a second stanza. In order to load them into our sketch, we made use of the loadJSON(); syntax. Although it seemed quite easy after our lecture and seeing videos about the topic, we couldn’t get this new concept to work right away. Luckily, our group members were able to help us and we eventually found out what the error was. 
Other than that, we tried to keep it simple and shift the focus to the concept and especially the text instead of the visuals, to not get the readers distracted. That being said, the technical part didn’t go too smooth and we had to make some sacrifices along the way to get our program to run. The ideation process, exchange of ideas and generally finding out what exactly we wanted to do took up a big chunk of our time working together. We knew from the get-go that we wanted to use the data in a randomized way in the form of a poem, complementing this week’s theme of e-literature. We’ve therefore used the random syntax to retrieve random sentences from the json-file every time the page is loaded. We both had multiple ideas, but fusing them together and reaching the same vision can be difficult at times, although they seemed compatible. It’s a “mini” exercise after all, so there is somewhat of a need to get more definitive. Communication is extremely important, which is something we once again were reminded of. It is just as necessary to share discontent and feeling stuck as it is to talk about new plans and exciting concepts. Remember to always encourage one another! We also used Atom’s “teletype” package for the first time and learned what it was like to code together. This feature had many errors, but is something we might continue to utilise in the future nonetheless. 


### Analysis on vocable code
> “[...], choosing meaningful identifier names is more for the purpose of expression and communication [...]. This is where we hear the programmer’s voice.” (Soon Winnie & Cox, Geoff, "Vocable Code", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, p. 182)

When naming the different files, a programmer makes a choice. The given name doesn’t affect the readability of the program for the computer, since it only has to be given two of the same values in order to run it properly, but it reflects the programmer’s choices and intentions. The programmer is the one who gives the file (or variable etc.) its meaning. We actively chose to call our json-files level1, level2 and so on. To “level up” is often related to improvement, progress and getting closer to a certain goal, a truth if you will. But advancing in levels in our e-lit piece doesn’t necessarily complete a picture, but rather makes you question everything more and more. In that way it is highly paradoxical. Our piece therefore is meant to pose questions about reality, because it is first when one questions reality, they can be stripped of the reality inflicted upon them.


### References and inspiration
#### Concept and visuals
[Seatlle Drift](http://www.vispo.com/animisms/SeattleDriftEnglish.html#)

[Vocable code](https://dobbeltdagger.net/VocableCode_Educational/)

#### Quotes etc.
[Viktor Orbán on children's book featuring homosexual character](https://www.euronews.com/2020/10/05/leave-our-children-alone-hungarian-pm-tells-publisher-of-lgbt-book)

[Interview with Viktor Orbán](https://miniszterelnok.hu/interview-with-viktor-orban-on-polsat-tv-channel/)

[József Szájer gay orgy](https://www.euractiv.com/section/politics/news/viktor-orban-condemns-mep-over-gay-orgy-donald-tusk-hints-at-expelling-fidesz-from-epp/)

[Socrates exchange](https://www.nhpr.org/post/socrates-exchange-are-all-our-beliefs-merely-opinions-or-are-there-some-universal-truths#stream/0)

[Danish People's Party (Dansk folkeparti) on banning gender studies at universities](https://danskfolkeparti.dk/wp-content/uploads/2021/04/Kulturudspil.pdf) (In Danish)

Various philosophers...
